class ImageUploader < CarrierWave::Uploader::Base  
  
  include CarrierWave::MiniMagick
  process resize_to_limit: [400, 400]
  process resize_to_fill:  [400, 400, "Center"]

  if Rails.env.production?
    storage :fog
  else
    storage :file
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}"
  end

  def extension_whitelist
    %w(jpg jpeg png)
  end
  
  def default_url
    [version_name, "default.jpg"].compact.join('_')
  end
end
