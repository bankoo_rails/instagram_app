class Notification < ApplicationRecord
  # Postモデルとの関係性
  belongs_to :post
  
  # Userモデルとの関係性
  belongs_to :user
  
  # バリデーション
  validates :user_id, presence: true
  validates :post_id, presence: true
end