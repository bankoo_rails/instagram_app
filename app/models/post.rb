class Post < ApplicationRecord
  # Userモデルとの関係性
  belongs_to :user
  
  # Likeモデルとの関係性
  has_many :likes, dependent: :destroy
  has_many :like_users, through: :likes, source: :post
  
  # Commentモデルとの関係性
  has_many :comments, dependent: :destroy
  
  # Notificationモデルとの関係性
  has_many :notification, dependent: :destroy
  
  # Postモデルに画像を追加する
  mount_uploader :picture, PictureUploader
  
  #バリデーション
  validates :picture, presence: true
  validates :user_id, presence: true
end