class Comment < ApplicationRecord
  # Userモデルとの関係性
  belongs_to :user
  
  # Postモデルとの関係性
  belongs_to :post
  
  # Notificationモデルとの関係性
  has_many :notification, dependent: :destroy
  
  #バリデーション
  validates :content, presence: true, length: {maximum: 200}
  validates :user_id, presence: true
  validates :post_id, presence: true
end
