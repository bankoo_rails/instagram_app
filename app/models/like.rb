class Like < ApplicationRecord
  # Userモデルとの関係性
  belongs_to :user
  
  # Postモデルとの関係性
  belongs_to :post
  
  # バリデーション
  validates :user_id, presence: true
  validates :post_id, presence: true
end