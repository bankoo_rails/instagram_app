class User < ApplicationRecord
  # Userモデルに画像を追加する
  mount_uploader :image, ImageUploader
  
  # Postモデルとの関係性
  has_many :posts, dependent: :destroy
  
  # Likeモデルとの関係性
  has_many :likes, dependent: :destroy
  has_many :like_posts, through: :likes, source: :post
  
  # Commentモデルとの関係性
  has_many :comments, dependent: :destroy
  
  # Notificationモデルとの関係性
  has_many :notification, dependent: :destroy
  
  # Deviseのモジュール設定
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable

  # バリデーション
  validates :name,  presence: true, length: {maximum: 20}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255}, uniqueness: true,
            format: { with: VALID_EMAIL_REGEX }
  validates :bio,   length: {maximum: 100}
  
  # フォロー機能
  acts_as_followable
  acts_as_follower
  
  # current_passwordを外部から読み書き可能にする
  attr_accessor :current_password
  
  # Facebookの情報をUserインスタンスに代入する
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name
      user.remote_image_url = auth.info.image
    end
  end

  # ユーザー登録に渡すデータを設定
  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"]) do |user|
        user.attributes = params
      end
    else
      super
    end
  end
end