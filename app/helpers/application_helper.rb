module ApplicationHelper
  # ページごとのタイトルを返す
  def full_title(page_title = '')
    base_title = "Insta App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
  
  # 以下はdeviceのヘルパーを他viewで使うための設定
  def resource_name
    :user
  end
 
  def resource_class
    User
  end
end
