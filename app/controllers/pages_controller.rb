class PagesController < ApplicationController
  def index # タイムライン
    if user_signed_in?
      @user = current_user
      @post = @user.posts.new
      @timeline_user = @user.all_following << @user
      @timeline = Post.all.where( user_id: @timeline_user)
                  .order( created_at: "DESC").page(params[:page]).per(9)
      @comment = Comment.new
    end
  end

  def about # 利用規約
  end
end