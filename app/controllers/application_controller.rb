class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # ストロングパラメータの設定
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_search #検索フォーム
  before_action :set_notification #通知
  before_action :set_username #ユーザーネームを代入
  
  def after_sign_in_path_for(resource) # ログイン後のリダイレクト先を分ける
    if current_user.all_following.present?
      root_path  # フォロワーがいればタイムライン
    else
      users_path # いなければおすすめユーザー一覧
    end
  end
  
  protected
    # ストロングパラメータを設定する
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :image,
                      :user_name, :website, :bio, :phonenumber, :gender])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name,
                      :user_name, :website, :bio, :phonenumber, :gender,
                      :image])
    end
  
  private
    # 検索ワードを取得
    def set_search
      @q = Comment.ransack(params[:q])
    end
    
    # ログインユーザーの通知を抽出する
    def set_notification
    @notification = Notification.where(post_id: current_user.posts.ids)
                    .order( created_at: "DESC") if user_signed_in?
    end
    
    # user_nameにnameを代入する
    def set_username
      if user_signed_in? && current_user.user_name.blank?
        current_user.update(user_name: current_user.name)
      end
    end
end