class PostsController < ApplicationController
  # ログインしていない場合ログインページにリダイレクト（Deviseヘルパー）
  before_action :authenticate_user!
  before_action :correct_user, only: :destroy
  
  def index # タイムライン
    @user = current_user
    @post = @user.posts.new
    @comment = Comment.new
    @q = Comment.ransack(params[:q])
    @timeline = Post.find @q.result.pluck(:post_id)
  end
  
  def create # 作成
    @post = current_user.posts.new(post_params)
    if @post.save
      redirect_to current_user, notice: "投稿に成功しました!"
    else
      # １つ前のURLを返す
      # その他のエラーメッセージにまだ対応できていない
      redirect_to request.referrer || root_url, alert: "写真が選択されていません。"
    end
  end

  def destroy # 削除
    @post.destroy
     # １つ前のURLを返す
    redirect_to request.referrer || root_url, notice: "削除に成功しました!"
  end
  
  private
    # Postモデルのpictureカラムをストロングパラメータに設定
    def post_params
      params.require(:post).permit(:picture) if params[:post].present?
    end
    
    # postのユーザーでなければHomeへリダイレクト
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end