class CommentsController < ApplicationController
  # ログインしていない場合ログインページにリダイレクト（Deviseヘルパー）
  before_action :authenticate_user!
  
  def create # コメント作成
    @comment = Comment.new(comment_params)
    if  @comment.update(user_id: current_user.id, post_id: params[:post_id])
      # 通知を作成する
      notice = Notification.new(user_id: @comment.user.id,
                                  post_id: @comment.post.id, kind: "comment")
      notice.save unless notice.user == notice.post.user
      flash[:notice] = '投稿にコメントしました'
    else
      flash[:alert]  = '投稿に失敗しました'
    end
    # renderで入力情報を引き継ぎたいが、複数ページからコメントできるため、難しい。
    redirect_to request.referrer || root_url
  end
  
  def destroy #コメント削除
    comment  = Comment.find(params[:id])
    comment.destroy if comment.present?
    # 通知を削除する
    notification = Notification.find_by(
                    user_id: current_user.id, post_id: comment.post.id)
    notification.destroy if notification.present?
    redirect_to request.referrer || root_url, notice: '削除しました'
  end
  
  private
    # Commentモデルのcontentカラムをストロングパラメータに設定
    def comment_params
      params.require(:comment).permit(:content)
    end
end