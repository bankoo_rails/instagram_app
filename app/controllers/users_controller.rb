class UsersController < ApplicationController
  # ログインしていない場合ログインページにリダイレクト（Deviseヘルパー）
  before_action :authenticate_user!,
    only: [:show, :follow, :unfollow, :following, :followers, :notification]
  
  def index # おすすめユーザー一覧
    @user    = current_user
    # 自分とフォローユーザーを除いた一覧
    @users   = User.where.not(id: @user.all_following.map(&:id) << @user.id)
                .page(params[:page]).per(6)
    @post    = @user.posts.new if user_signed_in?
    @comment = Comment.new
  end
  
  def show # 個別ページ
    @user    = User.find(params[:id])
    @post    = @user.posts.new if user_signed_in?
    @posts   = @user.posts.order( created_at: "DESC").page(params[:page]).per(9)
    @comment = Comment.new
  end
  
  def follow # フォロー
    @user = User.find(params[:id])
    current_user.follow(@user) # 対象ユーザーをフォロー
    # Ajaxリクエスト
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
  
  def unfollow # フォロー解除
    @user = User.find(params[:id])
    current_user.stop_following(@user) # 対象ユーザーのフォロー解除
    # Ajaxリクエスト
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
  
  def following  # フォロー一覧
    @user  = User.find(params[:id])
    # all_followingでarrayクラスを呼び出すため複雑になってしまっている
    @users = User.where(id: (@user.all_following.map(&:id)))
              .page(params[:page]).per(6)
    @post  = current_user.posts.new if user_signed_in?
  end
  
  def followers # フォロワー一覧
    @user  = User.find(params[:id])
    # followersでarrayクラスを呼び出すため複雑になってしまっている
    @users = User.where(id: (@user.followers.map(&:id)))
              .page(params[:page]).per(6)
    @post  = current_user.posts.new if user_signed_in?
  end
  
  def notification # 通知一覧
    @user    = current_user
    @users   = User.all
    @post    = @user.posts.new if user_signed_in?
    @comment = Comment.new
    @notification = Notification.where(post_id: @user.posts.ids)
                    .order( created_at: "DESC").page(params[:page]).per(6)
    # 開いたとき既読にする
    @notification.update(status: false) 
  end
end