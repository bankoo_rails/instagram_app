class LikesController < ApplicationController
  # ログインしていない場合ログインページにリダイレクト（Deviseヘルパー）
  before_action :authenticate_user!

  def show # お気に入り投稿一覧
    @user  = current_user
    @post  = current_user.posts.new if user_signed_in?
    @posts = @user.like_posts.order( created_at: "DESC")
    @comment = Comment.new
  end
  
  def create # 作成
    @post = Post.find(params[:post_id])
    # お気に入りする
    like = Like.new(user_id: current_user.id, post_id: @post.id)
    like.save if like.present?
    # 通知を作成する(セルフは除く)
    notification = Notification.new(user_id: current_user.id, post_id: @post.id, kind: 'like')
    notification.save unless notification.post.user == current_user
    # Ajaxリクエスト
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end

  def destroy # 削除
    @post = Post.find(params[:post_id])
    # お気に入り解除する
    like = Like.find_by(user_id: current_user.id, post_id: @post.id)
    like.destroy if like.present?
    # 通知を削除する
    notification = Notification.find_by(user_id: current_user.id, post_id: @post.id, kind: 'like')
    notification.destroy if notification.present?
    # Ajaxリクエスト
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end
end