class AddDatailsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :user_name, :string
    add_column :users, :website, :string
    add_column :users, :bio, :text
    add_column :users, :phonenumber, :string
    add_column :users, :gender, :string
  end
end
