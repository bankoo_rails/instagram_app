class ChangeColumnUser < ActiveRecord::Migration[5.2]
  def up
    change_column_default :users, :image, nil
  end
  
  def down
    change_column_default :users, :image, "default_user.png"
  end
end
