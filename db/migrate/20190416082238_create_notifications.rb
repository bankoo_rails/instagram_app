class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer  :user_id, null: false
      t.integer  :post_id, null: false
      t.string   :kind   , null: false
      t.boolean  :status,  default: true, null: false
      t.timestamps
    end
  end
end
