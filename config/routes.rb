Rails.application.routes.draw do
  root 'pages#index'
  get  'pages/about'

  #Devise
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations:      'users/registrations'
  }
  devise_scope :user do
    get 'password_edit', to: 'users/registrations#password_edit', as: 'password_edit'
    patch 'password_update', to: 'users/registrations#password_update', as: 'password_update'
  end
  
  #Usersリソース、follow/unfollowアクション
  resources :users, only: [:show, :index] do
    member do
      get :follow, :unfollow, :following, :followers, :notification
    end
  end
  
  #Postsリソース
  resources :posts, only: [:index, :create, :destroy]
  
  #Likesリソース
  resources :likes, only: [:show, :create, :destroy]
  
  #Commentsリソース
  resources :comments, only: [:create, :destroy]
end