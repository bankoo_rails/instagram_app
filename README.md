# Instagramクローンアプリ
このアプリはInstagramやTwitterなどの画像投稿サービスを参考に開発した、サンプルサイトです。  
Cloud 9にて構築を行い、本番環境ではAWSのS3ストレージで画像を管理しています。  

https://bankoo-instagram.herokuapp.com/

## 機能一覧
 - インフラ(Heroku, AWS S3)
 - ログイン機構（Devise, omniauth-facebook)
 - レスポンシブデザイン(bootstrap)
 - 画像投稿機能(carrierwave)
 - 画像検索機能(ransack)
 - モデルテスト（Rspec）
 - テンプレートエンジン(Slim)
 - ページネーション(kaminari)
 - お気に入り機能(Ajax)
 - フォロー機能　(Ajax)
 - コメント機能 
 - モーダル機能
 - 通知機能
