require 'rails_helper'

RSpec.describe 'Userモデル', type: :model do
  it '登録が有効' do
    user = User.new(name: 'foobar', email: 'foobar@example.com', password: 'foobar')
    expect(user).to be_valid
  end
  
  it 'nameが空のとき' do
    user = User.new(name: nil, email: 'foobar@example.com', password: 'foobar')
    user.valid?
    expect(user.errors[:name]).to include('を入力してください')
  end
  
  it 'emailが空のとき' do
    user = User.new(name: 'foobar', email: nil, password: 'foobar')
    user.valid?
    expect(user.errors[:email]).to include('を入力してください')
  end
  
  it 'nameが20文字より多いとき' do
    user = User.new(name: ("a" * 21))
    user.valid?
    expect(user.errors[:name]).to include('は20文字以内で入力してください')
  end
  
  it 'emailが255文字より多いとき' do
    user = User.new(email: "a" * 256 + '@example.com')
    user.valid?
    expect(user.errors[:email]).to include('は255文字以内で入力してください')
  end
  
  it 'bioが100文字より多いとき' do
    user = User.new(bio: ("a" * 101))
    user.valid?
    expect(user.errors[:bio]).to include('は100文字以内で入力してください')
  end
end